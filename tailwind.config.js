/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      container: {
        padding: "10px",
      },
      colors: {
        yellow: {
          400: "#fff300;",
        },
      },
    },
  },
  plugins: [],
};
