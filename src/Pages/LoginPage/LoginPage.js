import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { useEffect } from "react";
import { userService } from "./../../services/userService";
import { useNavigate } from "react-router";
import { useDispatch } from "react-redux";
import userSlice, { setUserInfo } from "../../redux-toolkit/userSlice";
import { localService } from "./../../services/localService";
import Lottie from "lottie-react";
import bg_animate from "../../assets/9103-entertainment.json";
export default function LoginPage() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const onFinish = (values) => {
    userService
      .postLogin(values)
      .then((res) => {
        message.success("Successfully Login!");

        dispatch(setUserInfo(res.data.content));

        localService.set(res.data.content);
        setTimeout(() => {
          window.location.href = "/";
        }, 1000);
      })

      .catch((err) => {
        console.log(err);
        message.error("Login Failed", err);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
    message.error("Login Failed");
  };

  return (
    <div className="flex flex-col items-center  justify-center h-screen overflow-hidden text-white">
      <div className="container mx-auto text-center">
        <h1 className="text-7xl pt-28 font-bold">Login</h1>
      </div>

      <div className="mx-auto flex  items-center w-full  ">
        <div className="container flex items-center mb-10">
          <div className="w-1/2">
            <Lottie animationData={bg_animate} loop={true} />
          </div>
          <div className="w-1/2">
            <Form
              name="basic"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 16,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                style={{ color: "white !important" }}
                label="Username"
                name="taiKhoan"
                rules={[
                  {
                    required: true,
                    message: "Please input your username!",
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                style={{ color: "white" }}
                label="Password"
                name="matKhau"
                rules={[
                  {
                    required: true,
                    message: "Please input your password!",
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item
                name="remember"
                valuePropName="checked"
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Checkbox style={{ color: "#fff" }}>Remember me</Checkbox>
              </Form.Item>

              <Form.Item
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Button
                  block
                  className="bg-lime-600 text-white"
                  htmlType="submit"
                >
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}
