import React, { useEffect, useState } from "react";
import { movieService } from "./../../services/movieService";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;
export default function MovieItem() {
  const [movieArr, setMovieArr] = useState([]);
  let renderMovieList = (movieArr) => {
    return movieArr.map((item) => {
      if (item.dangChieu == true) {
        return (
          <div className="flex justify-center items-center">
            <Card
              bodyStyle={{ padding: "0" }}
              style={{
                width: "80%",
                height: "100%",
                backgroundColor: "#000",
                border: "#000",
                hover: "transform: scale(50%)",
              }}
              cover={
                <div className="  overflow-hidden">
                  <NavLink to={`/detail/${item.maPhim}`}>
                    <img
                      hoverable
                      style={{
                        width: "100%",
                        transition: "all 1s ease",
                        verticalAlign: "middle",
                      }}
                      className="h-40 xl:h-80 object-cover"
                      alt="example"
                      src={item.hinhAnh}
                    />
                  </NavLink>
                </div>
              }
            >
              <h2 className="text-white text-base xl:text-lg text-center xl:text-left pt-5">
                {item.tenPhim}
              </h2>
            </Card>
          </div>
        );
      } else {
        return;
      }
    });
  };

  useEffect(() => {
    movieService
      .getMovieList()
      .then((res) => {
        setMovieArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div className="container grid grid-cols-3 lg:grid-cols-5 gap-x-1 gap-y-10 pt-10">
      {renderMovieList(movieArr)}
    </div>
  );
}
