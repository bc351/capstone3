import moment from "moment/moment";
import React from "react";
import "moment/locale/vi";
moment.locale("vi");

export default function MovieItemTab({ movie }) {
  return (
    <div className="flex mt-5 space-x-5 ">
      <img className="h-40 w-28 object-cover" src={movie.hinhAnh} alt="" />
      <div>
        <h3 className="text-lg xl:text-2xl ">{movie.tenPhim}</h3>
        <div className="grid grid-cols-2 xl:grid-cols-3 gap-7 mt-5">
          {movie.lstLichChieuTheoPhim.slice(0, 6).map((synopsis, index) => {
            return (
              <span
                key={index}
                className="bg-red-500 text-white text-sm xl:text-base px-2 py-2"
              >
                {moment(synopsis.ngayChieuGioChieu).format("DD/MM/YYYY ~ A")}
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}
