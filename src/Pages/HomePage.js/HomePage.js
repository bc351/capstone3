import React from "react";
import Banner from "../../components/Banner/Banner";
import MovieItem from "./MovieItem";
import MovieList from "./MovieList";
import MovieTab from "./MovieTab";
import MovieTabShowTime from "./MovieTabShowTime";

export default function HomePage() {
  return (
    <div className="bg-black  ">
      <Banner />

      <MovieList />
      <MovieTab />

      <MovieTabShowTime />
    </div>
  );
}
