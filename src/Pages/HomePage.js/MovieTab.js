import React from "react";
import { Table } from "antd";
import { useState, useEffect } from "react";
import { movieService } from "../../services/movieService";
import { MovieColumns } from "./ultils/ultils";

const columns = [
  {
    title: "Rating",
    dataIndex: "rating",
    key: "key",
    render: (text) => <a>{text}</a>,
  },
  {
    title: "Name",
    dataIndex: "name",
    key: "key",
  },
];
const data = [
  {
    key: "1",
    name: "John Brown",
    age: 32,
    address: "New York No. 1 Lake Park",
    tags: ["nice", "developer"],
  },
  {
    key: "2",
    name: "Jim Green",
    age: 42,
    address: "London No. 1 Lake Park",
    tags: ["loser"],
  },
  {
    key: "3",
    name: "Joe Black",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["cool", "teacher"],
  },
];
let newArrUseForSplice = [];
let arr = [];
let newArr = [];
let newMovieArr = [];
let hotMovie = [];
export default function MovieTab() {
  const [remainingMovieHotArr, setRemainingMovieHotArr] = useState([]);
  const [movieHotArr, setMovieHotArr] = useState([]);
  useEffect(() => {
    movieService
      .getMovieList()
      .then((res) => {
        checkMovieHot(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let checkMovieHot = (movieArr) => {
    let index = 0;
    return movieArr.map((item) => {
      if (item.hot == true) {
        index++;
        let newMovie = { ...item, index };
        arr.push(newMovie);
        newArrUseForSplice = arr.slice(0, 10);
        newMovieArr = newArrUseForSplice.splice(0, 5);
        setMovieHotArr(newMovieArr);
        console.log("arr: ", arr);
        newArr = arr.slice(5, 10);

        return setRemainingMovieHotArr(newArr);
      } else {
        return;
      }
    });
  };
  return (
    <div className="container mx-auto">
      <div className="flex justify-center items-start  space-x-10 mb-28 ">
        <div
          className="w-full
         lg:w-2/4"
        >
          <h1 className="text-white text-2xl lg:text-5xl py-10">
            Top Hot Movies
          </h1>
          <div className="flex justify-between gap-10">
            <Table
              showHeader={false}
              hoverable={true}
              className="text-white text-xs lg:text-base  bg-black"
              columns={MovieColumns}
              dataSource={movieHotArr}
              pagination={false}
            />
            <Table
              showHeader={false}
              hoverable={true}
              className="text-white text-xs lg:text-base bg-black"
              columns={MovieColumns}
              dataSource={remainingMovieHotArr}
              pagination={false}
            />
          </div>
        </div>
        <div className="w-2/4 hidden lg:block">
          <h1 className="text-white text-5xl py-10 ">Offers & Promotions</h1>
          <img
            style={{ width: "55vw", height: "40vh", marginTop: "5px" }}
            src="./img/credit-card.png"
            alt="not found"
          />
        </div>
      </div>
    </div>
  );
}
