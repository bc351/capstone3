import React from "react";
import { Tabs } from "antd";
import MovieItem from "./MovieItem";
import MovieItemComingSoon from "./MovieItemComingSoon";
const onChange = (key) => {};
export default function MovieList() {
  return (
    <div className=" mb-28">
      <Tabs
        className="text-white items-center text-center   "
        defaultActiveKey="1"
        onChange={onChange}
        items={[
          {
            label: `Now Showing`,
            key: "1",
            children: <MovieItem />,
          },
          {
            label: `Coming Soon`,
            key: "2",
            children: <MovieItemComingSoon />,
          },
        ]}
      />
    </div>
  );
}
