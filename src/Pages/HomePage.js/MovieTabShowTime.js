import React, { useEffect, useState } from "react";
import { movieService } from "../../services/movieService";

import { Tabs } from "antd";
import MovieItemTab from "./MovieItemTab";
const onChange = (key) => {
  console.log(key);
};

export default function MovieTabShowTime() {
  const [dataMovie, setDataMovie] = useState([]);
  useEffect(() => {
    movieService
      .getMovieByTheater()
      .then((res) => {
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderMovieListByTheater = (theater) => {
    return theater.danhSachPhim.map((movie) => {
      return <MovieItemTab key={movie.maPhim} movie={movie} />;
    });
  };
  const renderTheaterBySystem = (theaterSystem) => {
    return theaterSystem.lstCumRap.map((theater) => {
      return {
        label: (
          <div className="w-60 text-base">
            <h5>{theater.tenCumRap}</h5>
            <p className="truncate">{theater.diaChi}</p>
          </div>
        ),
        key: theater.maCumRap,
        children: (
          <div
            style={{ height: "33rem" }}
            className=" overflow-y-scroll  scrollbar"
          >
            {renderMovieListByTheater(theater)}
          </div>
        ),
      };
    });
  };
  const renderTheaterSystem = () => {
    return dataMovie.map((theaterSystem) => {
      return {
        label: <img className="w-16 h-16" src={theaterSystem.logo} alt="" />,
        key: theaterSystem.maHeThongRap,
        children: (
          <Tabs
            style={{ height: 550, color: "#fff" }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderTheaterBySystem(theaterSystem)}
          />
        ),
      };
    });
  };
  return (
    <div style={{}} className="movie-tab hidden lg:block   my-5 ">
      <div style={{ padding: "20px 50px" }} className="container mx-auto">
        <Tabs
          style={{
            height: 550,
            backgroundColor: "",
            color: "",
            border: "1px solid #fff",
          }}
          tabPosition="left"
          defaultActiveKey="1"
          onChange={onChange}
          items={renderTheaterSystem()}
        />
      </div>
    </div>
  );
}
