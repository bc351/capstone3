import React from "react";
import { useParams } from "react-router-dom";
import { movieService } from "./../../services/movieService";
import { useEffect, useState } from "react";
import moment from "moment/moment";

export default function DetailPage() {
  let params = useParams();

  const [movieDetail, setMovieDetail] = useState([]);
  const [time, setTime] = useState([]);
  let getMovieShowTime = (movieDetail) => {
    let time =
      movieDetail.heThongRapChieu[0]?.cumRapChieu[0]?.lichChieuPhim[0]
        .thoiLuong;
    if (movieDetail.heThongRapChieu.length == 0) {
      setTime("120");
    } else {
      setTime(time);
    }
  };
  useEffect(() => {
    movieService
      .getDetailMovie(params.id)
      .then((res) => {
        window.scrollTo(0, 0);

        setMovieDetail(res.data.content);
        getMovieShowTime(res.data.content);
      })

      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div
      style={{ paddingLeft: "40px" }}
      className=" text-white  flex justify-evenly px-8 py-5"
    >
      <div style={{ paddingRight: "50px" }} className="detail w-2/4   pt-5  ">
        <div className="mx-auto w-3/4">
          <div style={{ borderBottom: "2px solid #fff" }} className="content ">
            <h1 className="text-4xl text-yellow-400">{movieDetail.tenPhim}</h1>
            <h2
              style={{
                border: "2px solid grey",
                padding: "15px 30px",
                margin: "20px 0",
                transition: "0.5s",
                cursor: "pointer",
              }}
              className="text-2xl day"
            >
              Start day: {moment(movieDetail.ngayKhoiChieu).format("lll")}
            </h2>
            <span className="py-8 my-2 text-2xl">
              Rating: {movieDetail.danhGia}/10
            </span>
            <p
              style={{ margin: "10px 0" }}
              className="py-3 text-2xl text-white"
            >
              <i class="fa fa-clock"></i>: {time} minutes
            </p>
          </div>
          <div
            style={{
              paddingLeft: "15px",
              marginTop: "40px",
              width: "300px",
              height: "400px",
              marginBottom: "40px",
              overflow: "hidden",
              marginLeft: "90px",
            }}
            className="image "
          >
            <img
              style={{
                textAlign: "center",
                width: "90%",
                height: "100%",
              }}
              src={movieDetail.hinhAnh}
              alt=""
            />
          </div>

          <div
            style={{
              borderTop: "2px solid #fff",
              padding: "20px 10px",
              height: "50vh",
              overflow: "hidden",
              marginBottom: "50px",
            }}
            className="description"
          >
            <h2>Description: {movieDetail.moTa}</h2>
          </div>
        </div>
      </div>
      <div className="info w-2/3 h-screen flex items-center">
        <iframe
          style={{ padding: "50px 0" }}
          width="90%"
          height="90%"
          src={movieDetail.trailer}
          allowFullScreen="true"
        ></iframe>
      </div>
    </div>
  );
}
