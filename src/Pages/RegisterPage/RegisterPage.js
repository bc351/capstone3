import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { useEffect } from "react";
import { userService } from "./../../services/userService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { localService } from "../../services/localService";
import { setUserInfo } from "../../redux-toolkit/userSlice";
import Lottie from "lottie-react";
import bg_register from "../../assets/131930-progerss.json";
export default function RegisterPage() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const onFinish = (values) => {
    userService
      .postRegister(values)
      .then((res) => {
        message.success("Successfully registered!");
        let obj = res.data.content;
        const pick = (obj, ...args) => ({
          ...args.reduce((res, key) => ({ ...res, [key]: obj[key] }), {}),
        });
        let contentDispatch = pick(
          obj,
          "email",
          "hoTen",
          "maNhom",
          "soDT",
          "taiKhoan"
        );
        console.log("contentDispatch: ", contentDispatch);
        dispatch(setUserInfo(contentDispatch));
        localService.set(contentDispatch);

        setTimeout(() => {
          window.location.href = "/";
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className=" flex flex-col  justify-center h-screen overflow-hidden text-white">
      <div className="container mx-auto text-center">
        <h1 className="text-7xl pt-28">Register</h1>
      </div>

      <div className="mx-auto flex  items-center  ">
        <div className="container flex justify-center items-center">
          <div className="w-1/2">
            <Lottie animationData={bg_register} loop={true} />;
          </div>
          <div className="w-1/2">
            <Form
              name="basic"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 16,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                style={{ color: "white !important" }}
                label="Username"
                name="taiKhoan"
                rules={[
                  {
                    required: true,
                    message: "Please input your username!",
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                style={{ color: "white" }}
                label="Password"
                name="matKhau"
                rules={[
                  {
                    required: true,
                    message: "Please input your password!",
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>
              <Form.Item
                style={{ color: "white !important" }}
                label="Email"
                name="email"
                rules={[
                  {
                    required: true,
                    message: "Please input your valid email!",
                    type: "email",
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                style={{ color: "white !important" }}
                label="Phonenumber"
                name="soDT"
                rules={[
                  {
                    required: true,
                    message: "Please input your valid phone number!",
                    // type: 'number'
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                style={{ color: "white !important" }}
                label="Group Code"
                name="maNhom"
                rules={[
                  {
                    required: true,
                    message: "Please input your group code!",
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                style={{ color: "white !important" }}
                label="Full Name"
                name="hoTen"
                rules={[
                  {
                    required: true,
                    message: "Please input your full name!",
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="remember"
                valuePropName="checked"
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Checkbox style={{ color: "#fff" }}>Remember me</Checkbox>
              </Form.Item>

              <Form.Item
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Button
                  block
                  className="bg-lime-600 text-white"
                  htmlType="submit"
                >
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}
