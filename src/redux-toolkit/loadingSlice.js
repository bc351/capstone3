import { createSlice } from '@reduxjs/toolkit'

const initialState = {

}

const loadingSlice = createSlice({
  name: 'loadingSlice',
  initialState,
  reducers: {
    setLoadingOn: () => { 

     },
     setLoadingOff : () => { 
        
     },
  }
});

export const {} = loadingSlice.actions

export default loadingSlice.reducer