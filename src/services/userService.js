
import { https } from './configUrl';
export const userService={
    postLogin: (values) => { 
        return https.post('/api/QuanLyNguoiDung/DangNhap',values)
     },
    postRegister: (values) => { 
        return https.post('/api/QuanLyNguoiDung/DangKy', values)
     }
}