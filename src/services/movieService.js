import { https } from "./configUrl";

export const movieService = {
  getMovieList: () => {
    return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07");
  },
  getMovieComingSoonList: () => {
    return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07");
  },

  getDetailMovie: (id) => {
    return https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`);
  },
  getShowTime: (movieName) => {
    return https.get(
      `api/QuanLyPhim/LayDanhSachPhimTheoNgay?maNhom=GP07&tenPhim=${movieName}&soTrang=1&soPhanTuTrenTrang=10`
    );
  },
  getMovieTheaterInfo: () => {
    return https.get("/api/QuanLyRap/LayThongTinHeThongRap");
  },
  getBanner: () => {
    return https.get("/api/QuanLyPhim/LayDanhSachBanner");
  },
  getMovieByTheater: () => {
    return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP07");
  },
};

// api/QuanLyPhim/LayDanhSachPhimTheoNgay?maNhom=GP01&tenPhim=The%20Walking%20Dead&soTrang=1&soPhanTuTrenTrang=10
