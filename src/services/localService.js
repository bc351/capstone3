
export const DATA_JSON = "DATA_JSON"
export const localService={
    get : () => { 
        
        let data = localStorage.getItem(DATA_JSON)
        if(data){
      return JSON.parse(data)
        }else{
            return null
        }
     },
     set: (data) => { 
       let dataJson= JSON.stringify(data)
        localStorage.setItem(DATA_JSON,dataJson)
     },
     remove: () => { 
        localStorage.removeItem(DATA_JSON)
      }
}