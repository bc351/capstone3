
import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './Pages/HomePage.js/HomePage';
import DetailPage from './Pages/DetailPage/DetailPage';
import LoginPage from './Pages/LoginPage/LoginPage';
import RegisterPage from './Pages/RegisterPage/RegisterPage';
import Spinner from "./components/Spinner/Spinner";
import Layout from './HOC/Layout';

function App() {
  return (
    <div>
    <Spinner />
    <BrowserRouter>
    <Routes>
      <Route path='/' element={<Layout><HomePage/></Layout>}/>
      <Route path='/detail/:id' element={<Layout><DetailPage/></Layout>}/>
      <Route path='/login' element={<LoginPage/>}/>
      <Route path='/register' element={<RegisterPage/>}/>
    </Routes>
    </BrowserRouter>
    </div>
  )
}

export default App;
