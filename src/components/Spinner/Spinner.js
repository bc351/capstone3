import React from "react";
import { useSelector } from "react-redux";
import { PulseLoader } from "react-spinners";

export default function Spinner() {
  let isLoading = useSelector((state) => {
    return state.spinnerSlice.isLoading;
  });

  return isLoading ? (
    <div className="fixed bg-black w-screen h-screen top-0 l-0 z-50 flex justify-center items-center">
      <PulseLoader color="#36d7b7" />
    </div>
  ) : (
    <></>
  );
}
