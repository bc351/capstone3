import React, { useEffect, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
// import required modules
import { Pagination } from "swiper";
import { movieService } from "../../services/movieService";

export default function Banner() {
  let [bannerList, setBannerList] = useState([]);
  useEffect(() => {
    movieService
      .getBanner()
      .then((res) => {
        setBannerList(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderBannerList = () => {
    return bannerList.map((item) => {
      return (
        <SwiperSlide>
          <img className="swiper-img" src={item.hinhAnh} alt="" />
        </SwiperSlide>
      );
    });
  };
  return (
    <div className="">
      <Swiper
        pagination={{ clickable: true }}
        modules={[Pagination]}
        className="mySwiper"
      >
        {renderBannerList()}
      </Swiper>
    </div>
  );
}
