import React, { useEffect, useState } from "react";
import { movieService } from "../../services/movieService";

export default function Footer() {
  let [theaterInfo, setTheaterInfo] = useState([]);
  useEffect(() => {
    movieService
      .getMovieTheaterInfo()
      .then((res) => {
        setTheaterInfo(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderTheaterLogo = () => {
    return theaterInfo?.map((item) => {
      return (
        <li title="NOVO 7-STAR">
          <a href="/experience/1/NOVO-7-STAR" title="NOVO 7-STAR">
            <img
              alt="NOVO 7-STAR"
              title="NOVO 7-STAR"
              height={31}
              width={54}
              loading="lazy"
              src={item.logo}
              className=""
            />
          </a>
        </li>
      );
    });
  };
  return (
    <footer>
      <div className="container mx-auto">
        <div className="footer-top container flex justify-around mb-6">
          <div class="booking-block">
            <h2 className="text-sm xl:text-base">WAYS TO BOOK</h2>
            <ul class="n-appstore flex items-center ">
              <li className="mr-3 ">
                <a
                  class="n-ios-icon"
                  href="https://itunes.apple.com/in/app/novo-cinemas/id363121411?mt=8"
                  title="ios"
                  target="_blank"
                >
                  <img
                    data-src="/Content/NovoTheme/images/apple.svg"
                    alt="ios logo"
                    loading="lazy"
                    src="https://novouae-static.gumlet.io/Content/NovoTheme/images/apple.svg?w=70&dpr=1.3"
                    className="w-8"
                  />
                </a>
              </li>
              <li className="mr-3 ">
                <a
                  class="n-android-icon"
                  href="https://play.google.com/store/apps/details?id=com.grandcinema.gcapp.screens"
                  title="android"
                  target="_blank"
                >
                  <img
                    data-src="/Content/NovoTheme/images/android-logo.svg"
                    alt="android logo"
                    loading="lazy"
                    src="https://novouae-static.gumlet.io/Content/NovoTheme/images/android-logo.svg?w=100&dpr=1.3"
                    className="w-8"
                  />
                </a>
              </li>
              <li className="mr-3 ">
                <a
                  class="n-huawei-icon"
                  href="https://appgallery.cloud.huawei.com/marketshare/app/C101526647?locale=en_US&source=appshare&subsource=C101526647"
                  title="huawei"
                  target="_blank"
                >
                  <img
                    data-src="/Content/NovoTheme/images/Huawei-logo.png"
                    alt="huawei logo"
                    loading="lazy"
                    src="https://novouae-static.gumlet.io/Content/NovoTheme/images/Huawei-logo.png?w=100&dpr=1.3"
                    className="w-8"
                  />
                </a>
              </li>
            </ul>
          </div>
          <div class="n-exp-block">
            <h2 className="text-sm xl:text-base">EXPERIENCES</h2>
            <ul className="flex" id="experiences">
              {" "}
              {renderTheaterLogo()}
            </ul>
          </div>
          <div class="n-social-block">
            <h2 className="text-sm xl:text-base">CONNECT WITH US</h2>

            <ul class="n-social flex">
              <li className="mr-3">
                <a
                  class="n-fb"
                  href="https://www.facebook.com/novocinemas"
                  title="Connect through Facebook"
                >
                  <img
                    data-src="https://uae.novocinemas.com/Content/NovoTheme/images/facebook.svg"
                    alt="Facebook"
                    title="Facebook"
                    loading="lazy"
                    src="https://novouae-static.gumlet.io/Content/NovoTheme/images/facebook.svg?w=100&dpr=1.3"
                    class="w-8 h-8"
                  />
                </a>
              </li>
              <li className="mr-3">
                <a
                  class="n-fb"
                  href="https://twitter.com/NovoCinemas"
                  title="Connect through Twitter"
                >
                  <img
                    data-src="https://uae.novocinemas.com/Content/NovoTheme/images/twitter.svg"
                    alt="Twitter"
                    title="Twitter"
                    loading="lazy"
                    src="https://novouae-static.gumlet.io/Content/NovoTheme/images/twitter.svg?w=100&dpr=1.3"
                    class="w-8"
                  />
                </a>
              </li>
              <li className="mr-3">
                <a
                  class="n-fb"
                  href="https://www.instagram.com/NovoCinemas"
                  title="Connect through Instagram"
                >
                  <img
                    data-src="https://uae.novocinemas.com/Content/NovoTheme/images/insta.svg"
                    alt="Instagram"
                    title="Instagram"
                    loading="lazy"
                    src="https://novouae-static.gumlet.io/Content/NovoTheme/images/insta.svg?w=100&dpr=1.3"
                    class="w-8"
                  />
                </a>
              </li>
              <li className="mr-3">
                <a
                  class="n-fb"
                  href="https://www.youtube.com/user/NovoCinemas"
                  title="Connect through YouTube"
                >
                  <img
                    data-src="https://uae.novocinemas.com/Content/NovoTheme/images/youtube.svg"
                    alt="YouTube"
                    title="YouTube"
                    loading="lazy"
                    src="https://novouae-static.gumlet.io/Content/NovoTheme/images/youtube.svg?w=100&dpr=1.3"
                    class="w-8"
                  />
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="footer-body">
          <ul>
            <li>
              <a href="/Page?pagename=About-us" title="About us">
                About us
              </a>
            </li>
            <li>
              <a
                href="/Page?pagename=Advertise-with-Us"
                title="Advertise with Us"
              >
                Advertise with Us
              </a>
            </li>
            <li>
              <a href="/Page?pagename=Careers" title="Careers">
                Careers
              </a>
            </li>
            <li>
              <a href="/Page?pagename=Contact-Us" title="Contact Us">
                Contact Us
              </a>
            </li>
            <li>
              <a href="/Page?pagename=Privacy-Policy" title="Privacy Policy">
                Privacy Policy
              </a>
            </li>
            <br />
            <li>
              <a
                href="/Page?pagename=Terms-And-Conditions"
                title="Terms And Conditions"
              >
                Terms And Conditions
              </a>
            </li>
            <li>
              <a href="/promotions" title="Promotions">
                Promotions
              </a>
            </li>
            <li>
              <a href="/FAQs" title="FAQs">
                FAQs
              </a>
            </li>
          </ul>
        </div>
        <div className="footer-bot flex justify-between items-center">
          <div className="contact flex  items-center justify-center">
            <div className="subcribe mr-3">
              <p className="mb-2 text-left">Newsletter</p>
              <button>
                <a href="#">Subscribe Now</a>
              </button>
            </div>
            <div className="tak-with-us ">
              <p className="mb-2 text-left">Talk with Us</p>
              <button>
                <i class="fa fa-phone mr-2"></i>
                <a className="text-sm xl:text-base" href="#">
                  +971 6005 03328
                </a>
              </button>
            </div>
          </div>
          <div className="conditions">
            <p class="n-copyright">© Novo Cinemas 2023. All rights reserved.</p>
            <p>
              Clone by:{" "}
              <a
                className="text-red-400"
                href="https://www.facebook.com/addmeheree"
                target="_blank"
              >
                PHONG HUYNH
              </a>{" "}
              x{" "}
              <a
                className="text-red-400"
                href="https://www.facebook.com/alitangoxcute"
                target="_blank"
              >
                GIAO DANG
              </a>
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
}
