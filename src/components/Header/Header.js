import React from 'react'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { localService } from '../../services/localService'
import { headerCss, accountCss } from './headerCss'

export default function Header() {
  const user = useSelector((state)=>{
   return state.userSlice.user
  })
  let handleRemove = () => { 
    localService.remove()
    window.location.reload()
   }
  let displayContent = () => { 
    if(user){
      return <>
      <ul className='flex justify-start space-x-5'>
                <button className={accountCss}>{user?.hoTen}</button>
                <button className={headerCss} onClick={handleRemove}>Logout</button>
            </ul>
      </>
    }else{
      return <>
       <ul className='flex justify-start space-x-5'>
        <NavLink className={headerCss} to='/login'><li>Login</li></NavLink> 
        <NavLink className={headerCss}  to='/register'><li>Register</li></NavLink>
       </ul>
      </>
    }
   }
  return (
    <div style={{background:'#151515'}} className=' sticky top-0 z-50'>
        <div className="logo container py-5 mr-0 flex justify-between items-center  ">
        <NavLink to='/'><h1 style={{marginRight: '0'}} className='text-4xl text-yellow-400'>CyberFlix</h1></NavLink>
        <div className="info">
            {displayContent()}
        </div>
        </div>
       
    </div>
  )
}
